require "rails_helper"
RSpec.describe "medicalshops", :type => :request do
	context "when user go for ragistration" do
	    
	  it "registerd for medicalshops and redirects to the index page" do
	    get "new_medical_shop_path"
	    expect(response).to render_template(:new)
	      
	    post "/medicalshops", :params => { :medical_shop => {
	      :name => "mohan", 
	      :pincode => "452030", 
	      :state => "Madhya Pradesh",
	      :city => "Indore",
	      :area => "sudama Nagar",
	      :shop_address => "gopur colony",
	      :discount => 20,
	      :registerd_person_name => "asha",
	      :registration_number => "5",
	      :opening_time => "12",
	      :closing_time => "5",
	      :mobile_number => "9617269538"
	      }}
	
	    expect(response.status).to eq(302)
	    follow_redirect!
	  
	    expect(response).to render_template(:index)
	       # expect(response.body).to include("user was successfully created.")
	  end
	end
end