require "rails_helper"

RSpec.describe "user", :type => :request do
  context "when user go for ragistration" do
    context "and when user role is customer" do
      it "creates a user and redirects to the index page" do
      get "/users/sign_up?role=customer"
      expect(response).to render_template(:new)
      
        post "/users", :params => { :user => {
          :first_name => "My Widget",
          :last_name => "fgu",
          :mobile_number => 12345668789,
          :email => "v1@dfg.com",
          :password => 123456,

         :address_attributes => { :colony => "gopur", :district => "Indore", :pincode => "462030", :state => "Madhya Pradesh"} },
         :initial_role => "customer"}

        expect(response.status).to eq(302)
         follow_redirect!
  
        expect(response).to render_template(:index)
       # expect(response.body).to include("user was successfully created.")
      end
    end

    context "and when user role is shopkeeper" do
      it "creates a user and redirects to the index page" do
      get "/users/sign_up?role=shopkeeper"
      expect(response).to render_template(:new)
      
        post "/users", :params => { :user => {
          :first_name => "My Widget",
          :last_name => "fgu",
          :mobile_number => 12345668789,
          :email => "vj1@dfg.com",
          :password => 123456,

         :address_attributes => { :colony => "gopur", :district => "Indore", :pincode => "462030", :state => "Madhya Pradesh"} },
         :initial_role => "shopkeeper"
         }
        expect(response.status).to eq(302)
         follow_redirect!
  
        expect(response).to render_template(:index)
       # expect(response.body).to include("user was successfully created.")
      end
    end
  end

  it "does not render a different template" do
    get "/users/sign_in"
    expect(response).to_not render_template(:show)
  end
end