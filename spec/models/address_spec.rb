require 'rails_helper'

RSpec.describe Address, type: :model do
	  it { is_expected.to validate_presence_of(:district) }
	  it { is_expected.to validate_presence_of(:state) }
	  it { is_expected.to validate_presence_of(:pincode) }
    it { is_expected.to validate_presence_of(:colony) }

  describe 'associations' do
    it { should belong_to(:user)}
  end
  #pending "add some examples to (or delete) #{__FILE__}"
end


