require 'rails_helper'

# RSpec.describe Contact, type: :model do
#   pending "add some examples to (or delete) #{__FILE__}"
# end

RSpec.describe 'User' do           #

  it "is not valid without a email and less password" do
    user = User.new(email: nil, password: "fgh")
    expect(user).to_not be_valid
  end
  
  it "is not valid email without format(xyz@xyz.com) and with less password" do
    user = User.new(email: "nil", password: "fgh")
    expect(user).to_not be_valid
  end

  it "is not valid with less password" do
    user = User.new(email: "nil@xyz.com", password: "fgh")
    expect(user).to_not be_valid
  end

  it "is not valid email without format(xyz@xyz.com)" do
    user = User.new(email: "nil", password: "fgh123")
    expect(user).to_not be_valid
  end

  it "is valid with a email in format(xyz@xyz.com) and password with minimum 6" do
    user = User.new(email: "nil@yopmail.com", password: "123fgh", first_name: "monika", last_name: "verma", mobile_number: 121234561352)
    expect(user).to be_valid
  end

end 
# 	describe "An example of the comparison Matchers" do

#     it "should show how the comparison Matchers work" do
#       a = 1
#       b = 2
#       c = 3		
#       d = 'test string'
      
#       # The following Expectations will all pass
#       expect(b).to be > a
#       expect(a).to be >= a 
#       expect(a).to be < b 
#       expect(b).to be <= b 
#       expect(c).to be_between(1,3).inclusive 
#       expect(b).to be_between(1,3).exclusive 
#       expect(d).to match /TEST/i 
#     end
   
# 	end

# # RSpec.describe Article, :type => :model do
# #   it "is valid with valid attributes" do
# #   end

# #   it "is not valid without a title" do
# #     auction = Article.new(title: nil)
# #     expect(auction).to_not be_valid
# #   end

# #   it "is not valid without a description"
# #   it "is not valid without a start_date"
# #   it "is not valid without a end_date"
# # end
# end
