require 'rails_helper'

RSpec.describe MedicalShop, type: :model do
  context "when passing valid parameter" do 
	  it { is_expected.to validate_presence_of(:name) }
	  it { is_expected.to validate_presence_of(:discount) }
	  it { is_expected.to validate_presence_of(:mobile_number) }
    it { is_expected.to validate_presence_of(:opening_time) }
    it { is_expected.to validate_presence_of(:closing_time) }
    it { is_expected.to validate_presence_of(:registration_number) }
    it { is_expected.to validate_presence_of(:registerd_person_name) }
    it { is_expected.to validate_presence_of(:shop_address) }
    it { is_expected.to validate_presence_of(:area) }
    it { is_expected.to validate_presence_of(:city) }
    it { is_expected.to validate_presence_of(:state) }
    it { is_expected.to validate_presence_of(:pincode) }
  end
end
