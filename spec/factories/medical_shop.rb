FactoryBot.define do
  factory :medical_shop do
    name { "MyString" }
    pincode { "452030" }
    state {"Madhya Pradesh"}
    city {"Indore"}
    area { "sudama Nagar" }
    shop_address {"gopur colony"}
    discount {20}
    registerd_person_name { "asha" }
    registration_number { "5" }
    opening_time { "12" }
    closing_time { "5" }
    mobile_number { "9617269538" }
    user {FactoryBot.create(:user)}
    #verify {false}
    trait :verified do
      verify { true }
    end

    trait :not_verify do
      verify { false }
    end
  end
end