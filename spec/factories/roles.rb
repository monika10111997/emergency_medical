FactoryBot.define do
  factory :role do
    trait :admin do 
    	name {"admin"}
    end	

    trait :shopkeeper do 
    	name {"shopkeeper"}
    end	

    trait :customer do 
    	name {"customer"}
    end	
  end
end
