
FactoryBot.define do
  
  factory :user do
    first_name { "Monika-#{rand 100}" }
    last_name { "Verma-#{rand 100}" }
    email { "monika.v1em#{rand 100}@thoughtmines.com" }
    password { '123456' }
    mobile_number { '1234567890' }
    #status {true} 

    trait :unblock do 
    	status {true}
    end

    trait :blocked do 
    	status {false}
    end 
    #roles { FactoryBot.create(:role, :shopkeeper)}
    #userroles {"shopkeeper"}
  end
end

