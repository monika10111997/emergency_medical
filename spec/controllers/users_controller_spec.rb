require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  
  let(:user) { FactoryBot.create(:user)}
  let(:admin_role) { FactoryBot.create(:role, :admin)}
  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end

  context "when user is admin" do
		before(:each) do
		  	sign_in user 
		  	user.roles << admin_role
        get :index
    end    
    it "show all users" do   
      expect(response).to render_template("index")
    end 
    
  end
  
  let(:user_blocked) { FactoryBot.create(:user, :blocked)}
  let(:user_unblock) { FactoryBot.create(:user, :unblock)}

  describe "GET #user_block_unblock" do
  	context "when user is admin block to unblock user" do
			before(:each) do
		  	sign_in user 
		  	user.roles << admin_role
		  	user_blocked
        get :block_unblock_user, params: { user_id: user_blocked.id }
      end  
    	it "should status false" do  
    		expect(user_blocked.reload.status).to eq(true) 
    		expect(response.status).to eq(302)
    		#follow_redirect!
    	end 
  	end  
 
  	context "when user is admin and unblock to block user" do
			before(:each) do
		  	sign_in user 
		  	user.roles << admin_role
		  	user_unblock
        get :block_unblock_user, params: { user_id: user_unblock.id }
      end
      it "should status false" do  
    	  expect(user_unblock.reload.status).to eq(false) 
    	  expect(response.status).to eq(302)
    	#follow_redirect!
      end 
    end  
  end	
end


	