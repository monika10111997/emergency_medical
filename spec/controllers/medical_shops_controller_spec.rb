
require "rails_helper"
RSpec.describe MedicalShopsController, :type => :controller do

	let(:user) { FactoryBot.create(:user)}
  let(:shopkeeper_role) { FactoryBot.create(:role, :shopkeeper)}
  let(:admin_role) { FactoryBot.create(:role, :admin)}
	describe 'MedicalShops #create' do
		context "when user is loggedin" do
			context 'and when create with valid params' do
			  before(:each) do
			  	sign_in user 
	        
			  	user.roles << shopkeeper_role
			  	post :create, :params => { :medical_shop => {
			      :name => "mohan", 
			      :pincode => "462030", 
			      :image => "eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBIUT09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--9d4fa4d0f46616b7e6aa425f0c6ef71fe3881f7c",
			      :state => "Madhya Pradesh",
			      :city => "Indore",
			      :area => "sudama Nagar",
			      :shop_address => "gopur colony",
			      :discount => "20",
			     	:registerd_person_name => "asha",
			      :registration_number => "5",
			      :opening_time => "12",
			      :closing_time => "5",
			      :mobile_number => "9617269538",
			      :user_id => user.id
			      }}
			  end
			  it "registerd for medicalshops and redirects to the index page" do  
			    expect(response.status).to eq(302)
			  
			    expect(response).to render_template()
			    # expect(response.body).to include("user was successfully created.")
			  end
			end

			context 'and when create with invalid params' do
			before(:each) do
		  	sign_in user 
        
		  	user.roles << shopkeeper_role
		  	post :create, :params => { :medical_shop => {
		      :name => "mohan", 
		      :pincode => "", 
		      :image => "eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBIUT09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--9d4fa4d0f46616b7e6aa425f0c6ef71fe3881f7c",
		      :state => "Madhya Pradesh",
		      :city => "Indore",
		      :area => "sudama Nagar",
		      :shop_address => "gopur colony",
		      :discount => "20",
		     	:registerd_person_name => "asha",
		      :registration_number => "5",
		      :opening_time => "12",
		      :closing_time => "5",
		      :mobile_number => "9617269538",
		      :user_id => user.id
		      }}
		  end
		  it "registerd for medicalshops and redirects to the index page" do  
		    expect(response.status).to eq(200)
		  
		    expect(response).to render_template()
		    # expect(response.body).to include("user was successfully created.")
		  end
			end
    end
	end
  
  
  describe "GET #new" do
  	context "when user is loggedin" do
			before(:each) do
			  sign_in user 
    	   
			  user.roles << shopkeeper_role
    	  get :new
    	end    
    	it " shoult return  new_template" do   
    	  expect(response).to render_template()
    	end 
    
  	end
  end

  describe "GET #index" do
  	context "when user loggedin as an admin" do
			before(:each) do
			  	sign_in user 
    	    
			  	user.roles << shopkeeper_role
    	    get :index
    	end    
    	it "show all medicalshops" do   
    	  expect(response).to render_template("index")
    	end 
    
  	end
  end

  let(:medical_shop_verified) { FactoryBot.create(:medical_shop, :verified)}
  let(:medical_shop_not_verify) { FactoryBot.create(:medical_shop, :not_verify)}
  
  describe "GET #show" do
  	context "GET show/:id" do
    	it "show specific medicalshops details" do   
    	  get :show, params: { id: medical_shop_verified.id }
    		expect(response).to  render_template
    	end 
  	end
  end

  describe "GET #edit" do
  	context "when user is either owner of shop or admin" do 
  		before(:each) do
				sign_in user 
			     
				user.roles << admin_role
				medical_shop_verified
  		
    		get :edit, params: { id: medical_shop_verified.id }
    	end	
    	it "when user is admin edit medical shop" do
    		expect(response).to render_template
    	end	
    end		
  end

  describe "PATCH #update" do
  	context "if user is admin" do
  		before(:each) do
				sign_in user 
		     
				user.roles << admin_role
				medical_shop_verified
  	 # it "updates the medical_shop and redirects" do
  	    patch :update, params: { id: medical_shop_verified.id , medical_shop: { :name => "mohan", 
				  	   :pincode => "462030", 
				  	   :image => "eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBIUT09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--9d4fa4d0f46616b7e6aa425f0c6ef71fe3881f7c",
				  	   :state => "Madhya Pradesh",
				  	   :city => "Indore",
				  	   :area => "sudama Nagar",
				  	   :shop_address => "gopur colony",
				  	   :discount => "20",
				  	   :registerd_person_name => "asha",
				  	   :registration_number => "5",
				  	   :opening_time => "12",
				  	   :closing_time => "5",
				  	   :mobile_number => "9617269538",
				  	   :user_id => user.id}}
  	    expect(response).to be_redirect
  	  end
  	  it "should redirect with update" do
  	  	expect(response).to be_redirect
        expect( medical_shop_verified.reload.name).to eq('mohan')
  	  end	
  	end

  	context "if user is admin with invalid data in update" do
  		before(:each) do
				sign_in user 
		     
				user.roles << admin_role
				medical_shop_verified
  	 # it "updates the medical_shop and redirects" do
  	    patch :update, params: { id: medical_shop_verified.id , medical_shop: { :name => "mohin", 
				  	   :pincode => "", 
				  	   :image => "eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBIUT09IiwiZXhwIjpudWxsLCJwdXIiOiJibG9iX2lkIn19--9d4fa4d0f46616b7e6aa425f0c6ef71fe3881f7c",
				  	   :state => "Madhya Pradesh",
				  	   :city => "Indore",
				  	   :area => "sudama Nagar",
				  	   :shop_address => "gopur colony",
				  	   :discount => "20",
				  	   :registerd_person_name => "asha",
				  	   :registration_number => "5",
				  	   :opening_time => "12",
				  	   :closing_time => "5",
				  	   :mobile_number => "9617269538",
				  	   :user_id => user.id}}
  	  end
  	  it "should redirect with update" do
  	  	expect(response).to render_template(:edit)
        expect( medical_shop_verified.reload.name).not_to eq('mohin')
  	  end	
  	end
  end	

  describe "GET #search" do
  end	

  describe "GET #medical_shop_verification" do
  	context "when user is admin not verified_shop" do
			before(:each) do
			  sign_in user 
    	    
			  user.roles << admin_role
			  medical_shop_verified
    	  get :medical_shop_verification, params: { medical_shop_id: 
    	  	medical_shop_verified.id }
    	end  
  
    	it "should change verified to not verify" do  
    		expect(medical_shop_verified.reload.verify).to eq(false)
    		expect(response.status).to eq(302)
    	end 
  	end

  	context "when user is admin and not verify shop" do
			before(:each) do
			  sign_in user 
    	   
			  user.roles << admin_role
			  medical_shop_not_verify
    	  get :medical_shop_verification, params: { medical_shop_id: 
    	  	medical_shop_not_verify.id }
    	end  
  
    	it "should change not verify to verified" do  
    		expect(medical_shop_not_verify.reload.verify).to eq(true) 		 
    		expect(response.status).to eq(302)
    	end 
  	end
  end	
end