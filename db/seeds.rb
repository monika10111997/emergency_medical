# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# create a role named "admin"
# admin_role = Role.create!(:name => "admin")

# # create an admin user
# admin_user = User.create!(:email => "a6dmin@admin.com", :first_name => "monika", :last_name => "verma", :mobile_number => "1234567896", :password => "123456")

# # assign the admin role to the admin user.  (This bit of rails
# # magic creates a user_role record in the database.)
# admin_user.roles << admin_role

# 20.times {|i| Project.create!({title: "Project #{i + 1}"}) }