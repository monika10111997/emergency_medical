class AddShopAddressToMedicalShops < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_shops, :shop_address, :string
    add_column :medical_shops, :area, :string
    add_column :medical_shops, :city, :string
    add_column :medical_shops, :state, :string
    add_column :medical_shops, :pincode, :string
  end
end
