class AddMedicalIdToAddress < ActiveRecord::Migration[5.2]
  def change
  	add_column :addresses, :medical_id, :integer
  end
end
