class RenameMedicalsToMedicalShops < ActiveRecord::Migration[5.2]
  def change
  	rename_table :medicals, :medical_shops
  end
end
