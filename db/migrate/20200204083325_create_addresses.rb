class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :house_number
      t.string :colony
      t.string :landmark
      t.string :district
      t.string :state
      t.string :pincode
      t.integer :user_id
      t.timestamps
    end
  end
end
