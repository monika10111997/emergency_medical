class CreateMedicals < ActiveRecord::Migration[5.2]
  def change
    create_table :medicals do |t|
      t.string :name
      t.integer :mobile_nubmer
      t.string :opening_time
      t.string :closing_time
      t.string :registration_number
      t.string :registerd_person_name
      t.boolean :verify, default: :false
      t.decimal :discount, precision: 5, scale: 2
      t.timestamps
    end
  end
end
