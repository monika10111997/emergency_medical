class AddLatitudeToMedicalShops < ActiveRecord::Migration[5.2]
  def change
    add_column :medical_shops, :latitude, :float
    add_column :medical_shops, :longitude, :float
  end
end
