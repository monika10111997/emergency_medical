class AddUserIdToMedicalShops < ActiveRecord::Migration[5.2]
  def change
  	add_column :medical_shops, :user_id, :integer
  end
end
