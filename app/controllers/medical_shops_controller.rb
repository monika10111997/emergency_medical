class MedicalShopsController < ApplicationController
  before_action :set_medical_shop, only: [:show, :edit, :update]
  before_action :set_medical_shops, only: [:index, :medical_shop_verification]

	load_and_authorize_resource

	def new
		@medical_shop = MedicalShop.new
	end

	def create
		@medical_shop = MedicalShop.new(medical_shop_params)
    @medical_shop.image.attach(params[:medical_shop ][:image])
		return redirect_to root_path if @medical_shop.save
		flash[:alert] = @medical_shop.errors.full_messages.join(', ')
    render 'new'
	end

	def show
  end

  def edit 
    return redirect_to root_path unless @medical_shop.user_id == current_user.id or current_user.has_role? :admin
  end
  
  def update
    #debugger
    return redirect_to root_path unless @medical_shop.user_id == current_user.id or current_user.has_role? :admin
    if @medical_shop.update(medical_shop_params)
      flash[:notice] = "Medical Shop updated."
      redirect_to medical_shop_path(@medical_shop)
    else
      flash[:alert] = @medical_shop.errors.full_messages.join(', ')
      render :edit
    end
  end

  def index
  end

  def search
    if params[:search].blank? and params[:discount_min].blank? and 
      params[:discount_max].blank? and params[:open].blank? and params[:search_form].present?
      redirect_to(root_path, alert: "Empty field!") 
    else 
      if current_user.present? and current_user.has_role? :admin
        @medical_shopss = MedicalShop.all
        @medical_shopss = @medical_shopss.where(" ? BETWEEN opening_time AND closing_time ", Time.now.strftime("%H"))  if params[:open].present?
        @medical_shopss = @medical_shopss.where("discount >= ?", params[:discount_min])  if params[:discount_min].present?
        @medical_shopss = @medical_shopss.where("discount <= ?", params[:discount_max])  if params[:discount_max].present?
        @medical_shopss = @medical_shopss.where("lower(city) LIKE ? OR lower(name) LIKE ? OR 
           lower(area) LIKE ?", "%#{params[:search]}%", "%#{params[:search]}%",
            "%#{params[:search]}%") if params[:search].present?
      else  
        @medical_shopss = MedicalShop.all
        @medical_shopss = @medical_shopss.verified unless current_user.present? and current_user.has_role? :admin
        @medical_shopss = @medical_shopss.near([current_user.address.latitude, current_user.address.latitude], 50, :order => :distance) if current_user.present?
        @medical_shopss = @medical_shopss.where(" ? BETWEEN opening_time AND closing_time ", Time.now.strftime("%H"))  if params[:open].present?
        @medical_shopss = @medical_shopss.where("discount >= ?", params[:discount_min])  if params[:discount_min].present?
        @medical_shopss = @medical_shopss.where("discount <= ?", params[:discount_max])  if params[:discount_max].present?
        @medical_shopss = @medical_shopss.where("lower(city) LIKE ? OR lower(name) LIKE ? OR 
           lower(area) LIKE ?", "%#{params[:search]}%", "%#{params[:search]}%",
            "%#{params[:search]}%") if params[:search].present?
        @medical_shopss = MedicalShop.near(params[:my_location], 5, :order => :distance) unless current_user.present? and params[:my_location].blank?
      end
    end 
  end

  def medical_shop_verification
  	@medical_shop = MedicalShop.find(params[:medical_shop_id])
  	@medical_shop.verify = !@medical_shop.verify
  	@medical_shop.save
  	redirect_to medical_shops_path
  end
  

  private

  def set_medical_shop
    @medical_shop = MedicalShop.find(params[:id])
  end

  def set_medical_shops
    @medical_shops = MedicalShop.all
    @medical_shops = @medical_shops.verified unless current_user.present? and current_user.has_role? :admin
  end

  def medical_shop_params
    params.require(:medical_shop).permit( :user_id, :image, :latitude, :longitude, :name, :mobile_number, :opening_time, :closing_time, :registration_number, :registerd_person_name, :verify, :discount, :shop_address, :area, :city, :state, :pincode)
  end
end
