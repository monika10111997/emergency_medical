class ApplicationController < ActionController::Base
	protect_from_forgery with: :exception
  rescue_from CanCan::AccessDenied do |exception|
    flash[:alert] = "User not authorized to do this action."
    redirect_to root_path
  end
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters

  devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :mobile_number, :role, :status, :email, :password,
      address_attributes: [ :user_id, :house_number, :colony, :landmark, :district, :state, :pincode, :latitude, :longitude 
      ]
    ])

  devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :mobile_number, :role, :status, :email, :password,
      address_attributes: [ :user_id, :house_number, :colony, :landmark, :district, :state, :pincode, :latitude, :longitude 
      ]
    ])
  end

  def current_ability
    @current_ability ||= Ability.new(current_user)
  end
end
