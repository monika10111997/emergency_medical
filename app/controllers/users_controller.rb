class UsersController < ApplicationController
  def index
  	@users = User.all
  end

  def block_unblock_user
    @users = User.all
    @user = User.find(params[:user_id])
    @user.status = !@user.status
    @user.save
    redirect_to users_path
  end
end
