class User < ApplicationRecord
  # attr_reader :has_role

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_and_belongs_to_many :roles
  
  has_one :address, :dependent => :destroy
  has_one :medical_shop
  accepts_nested_attributes_for :address, allow_destroy: true
  validates :first_name, :last_name, presence: true
  validates :mobile_number, presence: true, numericality: { only_integer: true }, length: { in: 10..12 }

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  
  def has_role?(role_name)
    user_role = User.joins(:roles).where("email=? and roles.name=?",self.email, role_name)
    user_role.blank? ? false : true
  end
  
end
