class MedicalShop < ApplicationRecord
	validates :name, :mobile_number, :opening_time, :closing_time, :registration_number, :registerd_person_name, :discount, :shop_address, :area, :city, :state, :pincode, presence: true
  has_one_attached :image
  belongs_to :user
  after_validation :geocode 
	geocoded_by :pincode

	scope :verified,-> { where(verify: true) }
	scope :not_verified,-> { where(verify: false) }
end