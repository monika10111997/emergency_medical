class Address < ApplicationRecord
	belongs_to :user
	
	after_validation :geocode 
	geocoded_by :pincode # thiere should be full address
    #will use if needed
	validates :district, :state, :pincode, :colony, presence: true

	# def full_address
 #    [state, district, colony].compact.join(', ')
 #  end
end
