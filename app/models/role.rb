class Role < ApplicationRecord
	has_and_belongs_to_many :users
	validates :name, presence: true, inclusion: { in: %w(admin customer shopkeeper),
    message: "%{value} is not a valid role" }
end
