class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    
    user ||= User.new

    # role.name = "user"
  #user.roles.first.name
  #byebug
  
    if user.has_role?(:admin) #user.roles.first.name == "admin"
      can :manage, :all
    elsif user.has_role?(:shopkeeper) #user.roles.first.name == "shopkeeper" 
      #shopkeeper
      can :index, MedicalShop
      can :show, MedicalShop
      can :create, MedicalShop
      can :new, MedicalShop
      can :edit, MedicalShop
      can :update, MedicalShop
      can :search, MedicalShop
      #can :read, MedicalShop#
    elsif user.has_role?(:customer)
      can :index, MedicalShop
      can :show, MedicalShop
      can :search, MedicalShop
    else
      can :index, MedicalShop
      can :show, MedicalShop 
      can :search, MedicalShop 
    end
    
      # user ||= User.new # guest user (not logged in)
      # if user.admin?
      #   can :manage, :all
      # else
      #   can :nil, :all
      # end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end

  # def shopkeeper
  #   can [:new, :show, :index, :create, :edit], MedicalShopsController
  #   #can [], 
  # end

  # def customer
  #   can [:index, :show], MedicalShopsController
  # end
end
