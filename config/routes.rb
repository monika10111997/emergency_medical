Rails.application.routes.draw do
  
  
  devise_for :users, controllers: {registrations: 'users/registrations'}
  
  devise_scope :user do
    #resources :users, :only => [:show, :index] 
    #get "users_index", :to => "users/registrations#index" 
    get 'registration/index', :to => 'users/registrations#index'
    
  end

  get 'home/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"
  resources :medical_shops do
  	get 'medical_shop_verification', to: "medical_shop_verification"
  	collection do
    	get 'search'
  	end
  end

  resources :users do 
  	get 'block_unblock_user', :to => 'users#block_unblock_user'
  end
  #get 'users/registrations/:id' => 'users/registrations#show'
end
